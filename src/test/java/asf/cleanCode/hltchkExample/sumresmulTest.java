package asf.cleanCode.hltchkExample;

import org.junit.Test;

import static org.junit.Assert.*;

public class sumresmulTest {

    @Test
    public void test1() {
        int resultado = sumresmul.operacion("12 23 +");
        assertEquals(resultado, 35);
    }

    @Test
    public void test2() {
        int resultado = sumresmul.operacion("40 23 -");
        assertEquals(resultado, 17);
    }

    @Test
    public void test3() {
        int resultado = sumresmul.operacion("42 10 *");
        assertEquals(resultado, 420);
    }


}
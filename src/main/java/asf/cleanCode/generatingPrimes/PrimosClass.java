package asf.cleanCode.generatingPrimes;

import java.util.ArrayList;
import java.util.List;

public class PrimosClass
{
  public static List<Integer> generadorDePrimos(int limiteDeLaSucesionDePrimos)
  {
    if (limiteDeLaSucesionDePrimos >= 2)
    {
      int tamanyoArray = limiteDeLaSucesionDePrimos + 1;
      boolean[] booleanosTrueSiEsPrimo = new boolean[tamanyoArray];

      inicializacionDelArrayDeBooleanos(tamanyoArray, booleanosTrueSiEsPrimo);
      booleanosTrueSiEsPrimo[0] = booleanosTrueSiEsPrimo[1] = false;
      cribaConMultiplosNoPrimos(tamanyoArray, booleanosTrueSiEsPrimo);

      List<Integer> listaDeNumerosPrimos = new ArrayList<>();

      generacionDeListaFinalDePrimos(tamanyoArray, booleanosTrueSiEsPrimo, listaDeNumerosPrimos);

      return listaDeNumerosPrimos;
    }
    else
    return null;
  }

  private static void generacionDeListaFinalDePrimos(int tamanyoArray, boolean[] booleanosTrueSiEsPrimo, List<Integer> listaDeNumerosPrimos) {
    int i;
    int j;
    for (i = 0, j = 0; i < tamanyoArray; i++)
    {
      j = anyadirPrimoALista(booleanosTrueSiEsPrimo[i], listaDeNumerosPrimos, i, j);
    }
  }

  private static int anyadirPrimoALista(boolean b, List<Integer> listaDeNumerosPrimos, int i, int j) {
    if (b)
      listaDeNumerosPrimos.add(j++,i);
    return j;
  }

  private static void cribaConMultiplosNoPrimos(int tamanyoArray, boolean[] booleanosTrueSiEsPrimo) {
    int i;
    for (i = 2; i < Math.sqrt(tamanyoArray) + 1; i++)
    {
      comprobarMultiplosDePrimos(tamanyoArray, booleanosTrueSiEsPrimo, i);
    }
  }

  private static void comprobarMultiplosDePrimos(int tamanyoArray, boolean[] booleanosTrueSiEsPrimo, int i) {
    if (booleanosTrueSiEsPrimo[i])
    {
      actualizarValorSiMultiploNoEsPrimo(tamanyoArray, booleanosTrueSiEsPrimo, i);
    }
  }

  private static void actualizarValorSiMultiploNoEsPrimo(int tamanyoArray, boolean[] booleanosTrueSiEsPrimo, int i) {
    int j;
    for (j = 2 * i; j < tamanyoArray; j += i)
      booleanosTrueSiEsPrimo[j] = false;
  }

  private static void inicializacionDelArrayDeBooleanos(int tamanyoArray, boolean[] booleanosTrueSiEsPrimo) {
    int i;
    for (i = 0; i < tamanyoArray; i++)
      booleanosTrueSiEsPrimo[i] = true;
  }
}
package asf.cleanCode.wordFrequencyGame;

import java.util.*;

public class WordFrequencyGame {
    public String getResult(String originalString) {

        if (hasOnlyOneWord(originalString)) {
            return originalString + " 1";
        } else {


            String[] originalWordsArray = originalString.split("\\s+");
            List<Word> wordList = createWordsListFromOriginalWordsArray(originalWordsArray);
            Map<String, List<Word>> wordsMap = buildWordsMapFromWordsList(wordList);
            wordList = getSortedWordList(wordsMap);
            String resultado = buildResultString(wordList);
            return resultado;
        }
    }

    private String buildResultString(List<Word> wordList) {
        StringJoiner joiner = new StringJoiner("\n");
        for (Word w : wordList) {
            String s = w.getValue() + " " + w.getWordCount();
            joiner.add(s);
        }
        return joiner.toString();
    }

    private List<Word> getSortedWordList(Map<String, List<Word>> wordsMap) {
        List<Word> wordList;
        wordList = getWordsListWithoutRepetitions(wordsMap);

        wordList.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
        return wordList;
    }

    private List<Word> getWordsListWithoutRepetitions(Map<String, List<Word>> wordsMap) {
        List<Word> wordList;
        List<Word> list = new ArrayList<>();
        for (Map.Entry<String, List<Word>> entry : wordsMap.entrySet()) {
            Word word = new Word(entry.getKey(), entry.getValue().size());
            list.add(word);
        }
        wordList = list;
        return wordList;
    }

    private Map<String, List<Word>> buildWordsMapFromWordsList(List<Word> wordList) {
        //get the map for the next step of sizing the same word
        Map<String, List<Word>> map = new HashMap<>();
        for (Word word : wordList) {
            if (!map.containsKey(word.getValue())) {
                ArrayList ar = new ArrayList<>();
                ar.add(word);
                map.put(word.getValue(), ar);
            } else {
                map.get(word.getValue()).add(word);
            }
        }
        return map;
    }

    private List<Word> createWordsListFromOriginalWordsArray(String[] originalWordsArray) {
        List<Word> wordList = new ArrayList<>();
        for (String s : originalWordsArray) {
            Word word = new Word(s, 1);
            wordList.add(word);
        }
        return wordList;
    }

    private boolean hasOnlyOneWord(String originalString) {
        return originalString.split("\\s+").length == 1;
    }
}
